import 'package:equatable/equatable.dart';
import 'package:flutter_weather_bloc/constain/enum.dart';

class Weather extends Equatable {
  final WeatherCondition weatherCondition;
  final String formatedCondition;
  final double minTemp;
  final double temp;
  final double maxTemp;
  final int locationId;
  final String created;
  final DateTime lastUpdated;
  final String location;

  const Weather(
      {required this.weatherCondition,
      required this.formatedCondition,
      required this.minTemp,
      required this.temp,
      required this.maxTemp,
      required this.locationId,
      required this.created,
      required this.lastUpdated,
      required this.location});

  @override
  List<Object?> get props => [
        weatherCondition,
        formatedCondition,
        minTemp,
        temp,
        maxTemp,
        locationId,
        created,
        lastUpdated,
        location
      ];

  factory Weather.fromJson(dynamic objJson) {
    final consolidatedWeather = objJson["consolidated_weather"][0];
    return Weather(
        weatherCondition: _mapWeatherCondiion(consolidatedWeather['weather_state_abbr']) ?? "",
        formatedCondition: consolidatedWeather['weather_state_name'],
        maxTemp: consolidatedWeather['max_temp'] as double,
        minTemp: consolidatedWeather['min_temp'] as double,
        temp: consolidatedWeather['the_temp'] as double,
        locationId: objJson["woeid"] as int,
        created: consolidatedWeather["created"],
        lastUpdated: DateTime.now(),
        location: objJson['title']);
  }



  static _mapWeatherCondiion(String inputString) {
    Map<String, WeatherCondition> map = {
      "sn": WeatherCondition.snow,
      "sl": WeatherCondition.sleet,
      "h": WeatherCondition.hail,
      "t": WeatherCondition.thunderStorm,
      "hr": WeatherCondition.heavyRain,
      "lr": WeatherCondition.lightRain,
      "s": WeatherCondition.showers,
      "hc": WeatherCondition.heavyClound,
      "lc": WeatherCondition.lightCloud,
      "c": WeatherCondition.clear,
    };
    return map[inputString] ?? WeatherCondition.unknown;
  }
}
