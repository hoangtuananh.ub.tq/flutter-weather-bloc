import 'dart:async';

import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:flutter_bloc/flutter_bloc.dart';
import 'package:flutter_weather_bloc/bloc/theme_bloc.dart';
import 'package:flutter_weather_bloc/bloc/weather_bloc.dart';
import 'package:flutter_weather_bloc/events/theme_event.dart';
import 'package:flutter_weather_bloc/events/weather_event.dart';
import 'package:flutter_weather_bloc/screens/Setting%20Screen.dart';
import 'package:flutter_weather_bloc/screens/city_search.dart';
import 'package:flutter_weather_bloc/states/theme_state.dart';
import 'package:flutter_weather_bloc/states/weather_states.dart';

class WeatherScreen extends StatefulWidget {
  @override
  _WeatherScreenState createState() => _WeatherScreenState();
}

class _WeatherScreenState extends State<WeatherScreen> {
  late Completer<void> _completer;

  @override
  void initState() {
    _completer = Completer<void>();
    super.initState();
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(
        title: Text("Weather App"),
        actions: [
          IconButton(
              onPressed: () => goToSettingScreen(), icon: Icon(Icons.settings)),
          IconButton(
              onPressed: () async {
                String typed = await Navigator.push(
                    context,
                    MaterialPageRoute(
                        builder: (context) => CitySearchScreen()));
                if (typed != null) {
                  BlocProvider.of<WeatherBloc>(context)
                      .add(WeatherEventRequested(city: typed));
                }
              },
              icon: Icon(Icons.search_rounded))
        ],
      ),
      body: Center(
        child: BlocConsumer<WeatherBloc, WeatherState>(
          listener: (context, weatherstate) {
            if (weatherstate is WeatherStatSuccess) {
              BlocProvider.of<ThemeBloc>(context).add(ThemeEventWeatherChanged(
                  condition: weatherstate.weather.weatherCondition));
              // _completer?.complete();
              _completer = Completer();
            }
          },
          builder: (context, weatherState) {
            if (weatherState is WeatherStateLoading) {
              return Center(
                child: CircularProgressIndicator(),
              );
            } else if (weatherState is WeatherStatSuccess) {
              final weather = weatherState.weather;
              return BlocBuilder<ThemeBloc, ThemeState>(
                  builder: (context, themeState) {
                return RefreshIndicator(
                  onRefresh: () {
                    BlocProvider.of<WeatherBloc>(context)
                        .add(WeatherEventRefresh(city: weather.location));
                    return _completer.future;
                  },
                  child: Container(
                    color: themeState.bgColor,
                    child: ListView(
                      children: [
                        Column(
                          children: [
                            Text(weather.location),
                            Text(
                              "Max Temp: ${weather.maxTemp}",
                              style: TextStyle(color: themeState.textColor),
                            ),
                            Text(
                              "Min Temp: ${weather.minTemp}",
                              style: TextStyle(color: themeState.textColor),
                            ),
                            Text(
                              " Temp: ${weather.temp}",
                              style: TextStyle(color: themeState.textColor),
                            ),
                          ],
                        )
                      ],
                    ),
                  ),
                );
              });
            } else if (weatherState is WeatherStateFailure) {
              return Text(
                "Something went Wrong",
                style: TextStyle(color: Colors.red),
              );
            }
            return Text("SomeThing Went Wrong");
          },
          // builder:,
        ),
      ),
    );
  }

  goToSettingScreen() {
    Navigator.push(
        context, MaterialPageRoute(builder: (context) => SettingScreen()));
  }
}
