import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';

class CitySearchScreen extends StatefulWidget {
  @override
  _CitySearchScreenState createState() => _CitySearchScreenState();
}

class _CitySearchScreenState extends State<CitySearchScreen> {
  late TextEditingController txtController;

  @override
  void initState() {
    txtController = TextEditingController();
    super.initState();
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(
        title: Text("Enter City"),
      ),
      body: Form(
        child: Row(
          children: [
            Expanded(
                child: Padding(
              padding: EdgeInsets.only(left: 10.0),
              child: TextFormField(
                controller: txtController,
                decoration: InputDecoration(
                    labelText: "Enter a City", hintText: "Example Chicago"),
              ),
            )),
            IconButton(
                onPressed: () => Navigator.pop(context, txtController.text),
                icon: Icon(Icons.search))
          ],
        ),
      ),
    );
  }
}
