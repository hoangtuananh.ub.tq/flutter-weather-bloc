import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:flutter_bloc/flutter_bloc.dart';
import 'package:flutter_weather_bloc/bloc/setting_bloc.dart';
import 'package:flutter_weather_bloc/constain/enum.dart';
import 'package:flutter_weather_bloc/events/setting_event.dart';
import 'package:flutter_weather_bloc/states/setting_state.dart';

class SettingScreen extends StatelessWidget {
  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(
        title: Text("Setting"),
      ),
      body: ListView(
        children: [
          BlocBuilder<SettingBloc, SettingState>(
              builder: (context, settingState) {
            return ListTile(
              title: Text("Unit:"),
              isThreeLine: true,
              subtitle: Text(settingState.temperaUnit == TemperaUnit.celsius
                  ? "Celsius"
                  : "Fahrenheit"),
              trailing: Switch(
                value: settingState.temperaUnit == TemperaUnit.celsius,
                onChanged: (_) => BlocProvider.of<SettingBloc>(context)
                    .add(SettingToggleUnit()),
              ),
            );
          }),
        ],
      ),
    );
  }
}
