enum WeatherCondition {
  snow,
  sleet,
  hail,
  thunderStorm,
  heavyRain,
  lightRain,
  showers,
  heavyClound,
  lightCloud,
  clear,
  unknown
}
enum TemperaUnit { fahrenheit, celsius }
