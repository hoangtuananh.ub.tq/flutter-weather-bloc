import 'package:flutter/material.dart';
import 'package:flutter_bloc/flutter_bloc.dart';
import 'package:flutter_weather_bloc/constain/enum.dart';
import 'package:flutter_weather_bloc/events/theme_event.dart';
import 'package:flutter_weather_bloc/states/theme_state.dart';

class ThemeBloc extends Bloc<ThemeEvent, ThemeState> {
  ThemeBloc()
      : super(ThemeState(bgColor: Colors.lightBlue, textColor: Colors.white));

  @override
  Stream<ThemeState> mapEventToState(ThemeEvent event) async* {
    ThemeState newThemeState;
    if (event is ThemeEventWeatherChanged) {
      final condition = event.condition;
      if (condition == WeatherCondition.clear ||
          condition == WeatherCondition.lightCloud) {
        newThemeState =
            ThemeState(bgColor: Colors.yellow, textColor: Colors.black);
      } else if (condition == WeatherCondition.hail ||
          condition == WeatherCondition.snow ||
          condition == WeatherCondition.sleet) {
        newThemeState =
            ThemeState(bgColor: Colors.lightBlue, textColor: Colors.white);
      } else if (condition == WeatherCondition.heavyRain ||
          condition == WeatherCondition.lightRain ||
          condition == WeatherCondition.showers) {
        newThemeState =
            ThemeState(bgColor: Colors.indigo, textColor: Colors.white);
      } else if (condition == WeatherCondition.thunderStorm) {
        newThemeState =
            ThemeState(bgColor: Colors.deepPurple, textColor: Colors.white);
      }else{
        newThemeState =
            ThemeState(bgColor: Colors.lightBlue, textColor: Colors.white);
      }
      yield newThemeState;
    }
  }
}
