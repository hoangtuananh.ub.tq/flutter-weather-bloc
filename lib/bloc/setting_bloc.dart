import 'package:flutter_bloc/flutter_bloc.dart';
import 'package:flutter_weather_bloc/constain/enum.dart';
import 'package:flutter_weather_bloc/events/setting_event.dart';
import 'package:flutter_weather_bloc/states/setting_state.dart';

class SettingBloc extends Bloc<SettingEvent, SettingState>{
  SettingBloc(): super(SettingState(temperaUnit: TemperaUnit.celsius));

  @override
  Stream<SettingState> mapEventToState(SettingEvent event) async*{
    if(event is SettingToggleUnit){
      yield SettingState(temperaUnit: state.temperaUnit == TemperaUnit.celsius ? TemperaUnit.fahrenheit : TemperaUnit.celsius);
    }
  }

}