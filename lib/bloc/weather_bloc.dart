import 'package:flutter_bloc/flutter_bloc.dart';
import 'package:flutter_weather_bloc/events/weather_event.dart';
import 'package:flutter_weather_bloc/model/weather.dart';
import 'package:flutter_weather_bloc/repositories/weather_repository.dart';
import 'package:flutter_weather_bloc/states/weather_states.dart';

class WeatherBloc extends Bloc<WeatherEvent,WeatherState>{
  final WeatherRepository repository ;

  WeatherBloc({required this.repository}) : super(WeatherInital());



  @override
  Stream<WeatherState> mapEventToState(WeatherEvent event) async* {
    if(event is WeatherEventRequested){
      yield WeatherStateLoading();
      try{
        final Weather weather = await repository.getWeatherFromCity(event.city);
        yield WeatherStatSuccess(weather: weather);
      }catch(_){
        yield WeatherStateFailure();
      }
    } else if(event is WeatherEventRefresh){
      try{
        final Weather weather = await repository.getWeatherFromCity(event.city);
        yield WeatherStatSuccess(weather: weather);
      }catch(_){
        yield WeatherStateFailure();
      }
    }
  }

}