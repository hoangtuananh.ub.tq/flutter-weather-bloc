import 'package:flutter_bloc/flutter_bloc.dart';

class WeatherBlocObs extends BlocObserver{
@override
  void onEvent(Bloc bloc, Object? event) {
    // TODO: implement onEvent
    print("On Event: $event");
  }
  @override
  void onTransition(Bloc bloc, Transition transition) {
    // TODO: implement onTransition
    print("On Trans: $transition");
  }
  @override
  void onError(BlocBase bloc, Object error, StackTrace stackTrace) {
    // TODO: implement onError
    print("On Error: $error");
  }
}