import 'package:equatable/equatable.dart';

abstract class WeatherEvent extends Equatable {
  const WeatherEvent();
}

class WeatherEventRequested extends WeatherEvent {
  final String city;

  WeatherEventRequested({required this.city});

  @override
  List<Object?> get props => [city];
}

class WeatherEventRefresh extends WeatherEvent {
  final String city;

  WeatherEventRefresh({required this.city});

  @override
  List<Object?> get props => [city];
}
