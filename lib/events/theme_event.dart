import 'package:equatable/equatable.dart';
import 'package:flutter_weather_bloc/constain/enum.dart';

abstract class ThemeEvent extends Equatable{

}
class ThemeEventWeatherChanged extends ThemeEvent {
  final WeatherCondition? condition;

  ThemeEventWeatherChanged({required this.condition});

  @override
  List<Object?> get props => [condition];

}