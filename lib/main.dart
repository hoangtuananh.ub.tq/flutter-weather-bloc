import 'package:flutter/material.dart';
import 'package:flutter_bloc/flutter_bloc.dart';
import 'package:flutter_weather_bloc/bloc/setting_bloc.dart';
import 'package:flutter_weather_bloc/bloc/theme_bloc.dart';
import 'package:flutter_weather_bloc/bloc/weather_bloc.dart';
import 'package:flutter_weather_bloc/bloc/weather_bloc_observer.dart';
import 'package:flutter_weather_bloc/repositories/weather_repository.dart';
import 'package:flutter_weather_bloc/screens/weather_sceen.dart';

void main() {
  Bloc.observer = WeatherBlocObs();
  final WeatherRepository weatherRepository = WeatherRepository();
  runApp(MultiBlocProvider(
    providers: [
      BlocProvider<ThemeBloc>(create: (context) => ThemeBloc()),
      BlocProvider<SettingBloc>(create: (context) => SettingBloc())
    ],
    child: MyApp(
      weatherRepository: weatherRepository,
    ),
  ));
}

class MyApp extends StatelessWidget {
  final WeatherRepository weatherRepository;

  MyApp({Key? key, required this.weatherRepository});

  @override
  Widget build(BuildContext context) {
    return MaterialApp(
      title: 'Flutter Weather with Bloc',
      theme: ThemeData(
        primarySwatch: Colors.blue,
      ),
      home: BlocProvider(
        create: (context) => WeatherBloc(repository: weatherRepository),
        child: WeatherScreen(),
      ),
    );
  }
}
