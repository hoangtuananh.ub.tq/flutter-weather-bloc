import 'dart:convert';

import 'package:flutter_weather_bloc/model/weather.dart';
import 'package:http/http.dart' as http;

const BASE_URL = "https://www.metaweather.com/api";
final localtionUrl = (city) => "$BASE_URL/location/search/?query=$city";
final weatherUrl = (locationId) => "$BASE_URL/location/$locationId";

class WeatherRepository {
  final http.Client? httpClient = http.Client();

  WeatherRepository();

  Future<int> getLocationFromCity(String city) async {
    final response = await this.httpClient!.get(Uri.parse(localtionUrl(city)));
    if (response.statusCode == 200 || response.statusCode == 201) {
      final cities = jsonDecode(response.body) as List;
      return (cities.first)["woeid"] ?? 0;
    } else {
      throw Exception("Error getting location $city");
    }
  }

  Future<Weather> fetchWeather(int locationId) async {
    final response =
        await this.httpClient!.get(Uri.parse(weatherUrl(locationId)));
    if (response.statusCode != 200) {
      throw Exception("Error Getting Weather");
    } else {
      final weatherJson = jsonDecode(response.body);
      return Weather.fromJson(weatherJson);
    }
  }

  Future<Weather> getWeatherFromCity(String city) async {
    final int locationId = await getLocationFromCity(city);
    return await fetchWeather(locationId);
  }
}
