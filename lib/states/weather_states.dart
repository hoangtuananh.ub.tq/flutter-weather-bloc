import 'package:equatable/equatable.dart';
import 'package:flutter_weather_bloc/model/weather.dart';

abstract class WeatherState extends Equatable {
  const WeatherState();

  @override
  // TODO: implement props
  List<Object?> get props => [];
}

class WeatherInital extends WeatherState {}

class WeatherStateLoading extends WeatherState {}

class WeatherStatSuccess extends WeatherState {
  final Weather weather;

  WeatherStatSuccess({required this.weather});

  @override
  // TODO: implement props
  List<Object?> get props => [weather];
}

class WeatherStateFailure extends WeatherState {}
