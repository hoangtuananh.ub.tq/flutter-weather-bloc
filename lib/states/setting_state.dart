import 'package:equatable/equatable.dart';
import 'package:flutter_weather_bloc/constain/enum.dart';

 class SettingState extends Equatable {
  final TemperaUnit temperaUnit;

  SettingState({required this.temperaUnit});
  @override
  // TODO: implement props
  List<Object?> get props => [temperaUnit];
}
