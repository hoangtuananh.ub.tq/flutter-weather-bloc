import 'package:equatable/equatable.dart';
import 'package:flutter/cupertino.dart';

class ThemeState extends Equatable {
  final Color bgColor;
  final Color textColor;

  ThemeState({required this.bgColor, required this.textColor});

  @override
  // TODO: implement props
  List<Object?> get props => [bgColor,textColor];
}
